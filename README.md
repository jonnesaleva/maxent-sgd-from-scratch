# CS 134A - Statistical Methods for NLP

## Project Assignment #1

Author: Jonne Saleva (jonnesaleva@brandeis.edu)

This repository contains my submission for PA1 of CS134A.

### How to run
1. Make sure you have `yelp_reviews.json` in the current folder.
2. Run `init.sh` to create a data folder in the parent folder of this repo
3. Run `extract_features.py` to generate BOW features and label encodings for Names and Yelp Reviews corpora
4. Run `test_maxent.py`
5. Run `exp1_exp2_grid.py reviews` to run Experiments 1 and 2.
6. Run `exp3.py reviews` to run Experiment 3.
7. Run `create_csv.py` to transform result pickles to csvs
8. Run `plot.py` if you want to generate plots.
9. Explore plots in `./results/`

