# -*- mode: Python; coding: utf-8 -*-

"""For the purposes of classification, a corpus is defined as a collection
of labeled documents. Such documents might actually represent words, images,
etc.; to the classifier they are merely instances with features."""

from abc import ABCMeta, abstractmethod
from csv import reader as csv_reader
from glob import glob
import json
from os.path import basename, dirname, split, splitext
import numpy as np
import scipy as sp
import pickle
import helpers as h

class Document(object):
    """A document completely characterized by its features."""

    max_display_data = 10 # limit for data abbreviation

    def __init__(self, docid, data, parent_corpus, 
                 label=None, source=None, 
                 label_ix=None, label_onehot=None, 
                 feature_vector=None):

        self.docid = docid
        self.data = data
        self._label_str = label
        self._label_ix = label_ix
        self._label_onehot = label_onehot
        self.source = source

    @property
    def label(self):
        return self._label_str

    @label.setter
    def label(self, label):
        self._label_str = label

    @property
    def label_ix(self):
        return self._label_ix

    @label_ix.setter
    def label_ix(self, ix):
        self._label_ix = ix

    @property
    def label_onehot(self):
        return self._label_onehot\
                   .toarray()\
                   .ravel()

    @label_onehot.setter
    def label_onehot(self, onehot):
        self._label_onehot = onehot

    @property
    def feature_vector(self):
        return self.parent_corpus.data_matrix[:, self.docid]

    def __repr__(self):
        return ("<%s: %s>" % (self.label, self.abbrev()) if self.label else
                "%s" % self.abbrev())

    def abbrev(self):
        return (self.data if len(self.data) < self.max_display_data else
                self.data[0:self.max_display_data] + "...")

    def features(self):
        """A list of features that characterize this document."""
        # based on test_maxent.py this will be overridden
        return [self.data]

    def _stack_feature_vector(self, zero_out_incorrect=False):
        K = len(self._label_onehot.shape)
        docids = [self.docid for _ in range(K)]
        M = self.parent_corpus.data_matrix[:, docids].T
        if zero_out_incorrect:
            return h._row_wise_multiply(self.label_onehot, M)
        else:
            return M

class BagOfWords(Document):
    def features(self):
        """Trivially tokenized words."""
        return self.data.split()

class Name(Document):
    def features(self):
        name = self.data
        return ['First=%s' % name[0], 'Last=%s' % name[-1]] 

class Corpus(object):
    """An abstract collection of documents."""

    __metaclass__ = ABCMeta

    def __init__(self, datafiles, document_class=Document):
        self.documents = []
        self.datafiles = glob(datafiles)
        for datafile in self.datafiles:
            self.load(datafile, document_class)

    # Act as a mutable container for documents.
    def __len__(self): return len(self.documents)
    def __iter__(self): return iter(self.documents)
    def __getitem__(self, key): return self.documents[key]
    def __setitem__(self, key, value): self.documents[key] = value
    def __delitem__(self, key): del self.documents[key]

    @abstractmethod
    def load(self, datafile, document_class):
        """Make labeled document instances for the data in a file."""
        pass

class PlainTextFiles(Corpus):
    """A corpus contained in a collection of plain-text files."""

    def load(self, datafile, document_class):
        """Make a document from a plain-text datafile. The document is labeled
        using the last component of the datafile's directory."""
        label = split(dirname(datafile))[-1]
        with open(datafile, "r") as file:
            data = file.read()
            _doc = document_class(data=data, label=label, parent_corpus=self)
            self.documents.append(_doc)

class PlainTextLines(Corpus):
    """A corpus in which each document is a line in a datafile."""

    def load(self, datafile, document_class):
        """Make a document from each line of a plain text datafile.
        The document is labeled using the datafile name, sans directory
        and extension."""
        label = splitext(basename(datafile))[0]
        with open(datafile, "r") as file:
            for line in file:
                data = line.strip()
                self.documents.append(document_class(docid=None, 
                                                     parent_corpus=self,
                                                     data=data, 
                                                     label=label))

class NamesCorpus(PlainTextLines):
    """A collection of names, labeled by gender. See names/README for
    copyright and license."""


    def __init__(self, datafiles="names/*.txt", 
                       label_pickle_file=None, 
                       feature_pickle_file=None, 
                       feature_extraction_mode=False,
                       document_class=Document):

        if (not feature_extraction_mode and 
            not label_pickle_file and 
            not feature_pickle_file):
            feature_pickle_file = '../data/pa1/names_bow_features.pkl'
            label_pickle_file = '../data/pa1/names_labels.pkl'

        # set up cache for data matrix
        if feature_pickle_file:
            self.data_matrix = h.load_pickle(feature_pickle_file)

        # load indices and features
        if label_pickle_file:
            label_pickle = h.load_pickle(label_pickle_file)
            docid2label = label_pickle['ix2label']
            docid2label_ix = label_pickle['label_ixs']
            docid2label_onehot = label_pickle['labels_onehot']

        super(NamesCorpus, self).__init__(datafiles, document_class)
        for ix, doc in enumerate(self.documents):
            doc.docid = ix
            doc.parent_corpus = self
            if label_pickle_file:
                doc.label_ix = docid2label_ix[ix]
                doc.label_onehot = docid2label_onehot[ix]

class ReviewCorpus(Corpus):
    """
    Yelp dataset challenge. A collection of business reviews. 
    """

    def __init__(self, datafiles, 
                       label_pickle_file=None, 
                       feature_pickle_file=None, 
                       feature_extraction_mode=False,
                       document_class=Document):

        if (not feature_extraction_mode and 
            not label_pickle_file and 
            not feature_pickle_file):
            feature_pickle_file = '../data/pa1/reviews_bow_features.pkl'
            label_pickle_file = '../data/pa1/reviews_labels.pkl'

        self.documents = []
        self.datafiles = glob(datafiles)

        # set up cache for data matrix
        if feature_pickle_file:
            self.data_matrix = h.load_pickle(feature_pickle_file)
        else:
            self.data_matrix = None

        # load indices and features
        if label_pickle_file:
            label_pickle = h.load_pickle(label_pickle_file)
            docid2label = label_pickle['ix2label']
            docid2label_ix = label_pickle['label_ixs']
            docid2label_onehot = label_pickle['labels_onehot']
        else:
            label_pickle = None
        
        for datafile in self.datafiles:
            self.load(datafile, document_class)

        for ix, doc in enumerate(self.documents):
            doc.docid = ix
            doc.parent_corpus = self
            if label_pickle_file:
                doc.label_ix = docid2label_ix[ix]
                doc.label_onehot = docid2label_onehot[ix]

    def load(self, datafile, document_class):
        """Make a document from each row of a json-formatted Yelp reviews
        """
        with open(datafile, "r") as file:
            for line in file:
                review = json.loads(line)
                label = review['sentiment']
                data = review['text']
                _doc = document_class(docid=None, 
                                      data=data, 
                                      label=label, 
                                      parent_corpus=self)
                self.documents.append(_doc)

