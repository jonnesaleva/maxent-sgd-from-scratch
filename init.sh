echo "Installing requirements through pip..."
pip install -r requirements.txt
echo "Creating data folder..."
mkdir -p ../data/pa1/
echo "Moving yelp reviews there..."
mv yelp_reviews.json ../data/pa1/
echo "Creating symlink to fool Python"
ln -s ../data/pa1/yelp_reviews.json yelp_reviews.json
echo "All done!"
