# -*- mode: Python; coding: utf-8 -*-

from classifier import Classifier
from scipy.special import logsumexp
import matplotlib.pyplot as plt
import scipy.sparse as sps
import datetime as dt
import helpers as h
import pandas as pd
import numpy as np
import scipy as sp
import pickle 
import time
import uuid
import os

class MaxEnt(Classifier):

    def __init__(self, learning_rate=0.1, 
                       l2_penalty=0.0,
                       max_iter=200, 
                       tolerance_loss=1e-4, 
                       max_iter_early_stop=30, 
                       minibatch_size=30,
                       parameter_path='../data/pa1/.params/', 
                       results_path='./results/',
                       verbose=True, 
                       experiment_name=None):
        """
        Initializes a Maximum Entropy (aka softmax)
        classifier.

        Parameters
        ----------
        learning_rate : float

        l2_penalty : float
            lambda term multiplying L2 regularization penalty
            
            defaults to 0.0

        max_iter : int
            maximum number of minibatch iterations before stopping,
            even if the optimization hasn't converged.

            defaults to 10

        tolerance_loss : float
            lower bound on amount by which the dev set loss needs to
            change before the optimization is determined to have converged.

            defaults to 1e-4

        max_iter_early_stop : int
            maximum number of gradient descent iterations where the loss stays
            constant before the optimization is stopped

            defaults to 10

        minibatch_size : int 
            default size of each minibatch

        parameter_path : str
            path to a folder in which model parameters (theta matrix) will be saved.

        results_path : str
            path to a folder in which results like loss traces etc will be saved.

        verbose : bool

        experiment_name : str
            Optional name for the experiment to more easily identify it.
        """
        super().__init__()
        self.theta = None
        self.last_theta = None
        self.train_loss_cache = []
        self.dev_loss_cache = []
        self.dev_loss_improvement = []
        self.theta_norm_cache = []
        self.ix2label = {}
        self.VERBOSE = verbose

        # early stopping hyperparameters
        self.N_ITER = 0
        self.MAX_ITER = max_iter
        self.MAX_ITER_EARLY_STOP = max_iter_early_stop
        self.TOL_LOSS = tolerance_loss

        # other hyperparams
        self.MINIBATCH_SIZE = minibatch_size
        self.LEARNING_RATE = learning_rate
        self.L2_PENALTY = l2_penalty
        self.PARAM_PATH = parameter_path
        if experiment_name is None:
            self.EXPERIMENT_NAME = dt.datetime.now().strftime('%y%m%d-%H%M%S')
        else:
            self.EXPERIMENT_NAME = experiment_name

        self.RESULTS_PATH = results_path + self.EXPERIMENT_NAME

        for p in (self.PARAM_PATH, self.RESULTS_PATH):
            if not os.path.exists(p):
                os.system('mkdir -p {}'.format(p))

    def plot_results(self):
        """
        Plots loss traces and theta norms encountered during training.
        """
        
        train_loss_series = pd.Series(self.train_loss_cache)
        train_loss_average = train_loss_series.rolling(50).mean()
        train_df = pd.DataFrame({'train_loss': train_loss_series, 
                                 'window_50_mean': train_loss_average})

        if len(self.dev_loss_cache) > 0:
            dev_loss_series = pd.Series(self.dev_loss_cache)
            dev_loss_average = dev_loss_series.rolling(50).mean()
            dev_df = pd.DataFrame({'dev_loss': dev_loss_series, 
                                   'window_50_mean': dev_loss_average})

        theta_norm_series = pd.Series(self.theta_norm_cache)
        theta_norm_average = theta_norm_series.rolling(50).mean()
        theta_df = pd.DataFrame({'theta_norm': theta_norm_series, 
                               'window_50_mean': theta_norm_average})

        dfs = [('theta_norm.png', 'F-norm of parameters + 50-period MA', theta_df), 
               ('train_loss.png', 'Minibatch train loss + 50-period MA', train_df)]

        if len(self.dev_loss_cache) > 0:
               dfs.append(('dev_loss.png', 'Dev set loss + 50-period MA', dev_df))

        for fname, title, df in dfs:
            df.plot(title=title)
            plt.savefig(f'{self.RESULTS_PATH}/{fname}')
        
        plt.close()


    def truncate(self, theta, remove_intercept=False):
        """
        Gets rid of bias term for regularization, either
        by removing it explicitly (useful for norm calcs)
        or by zeroing it out (useful for gradient calcs)
        """
        if remove_intercept:
            out = theta[:, 1:]
        else:
            out = theta.copy()
            out[:, 0] = 0
        return out

    def get_model(self): return None

    def set_model(self, model): pass

    model = property(get_model, set_model)
    
    def save_training_stats(self):
        """
        Saves train and dev losses, as well as F-norm
        of parameter matrix theta into separate pickle files.
        """
        caches = [('theta_norm_cache', self.theta_norm_cache), 
                  ('dev_loss_cache', self.dev_loss_cache), 
                  ('train_loss_cache', self.train_loss_cache)]

        for cache_name, cache in caches:
            h.dump_pickle(cache, f'{self.RESULTS_PATH}/{cache_name}.pkl')

        if self.VERBOSE:
            print('Training stats saved!')

    def save_parameters(self):
        """
        Caches parameters as a pickle into self.PARAM_PATH
        """
        suffix = int(100*time.time())
        file_name = 'maxent_theta_{}.pkl'.format(suffix)
        full_path = '{}{}{}'.format(self.PARAM_PATH, 
                                    "" if self.PARAM_PATH.endswith("/") 
                                       else "/", 
                                    file_name)
        with open(full_path, 'wb') as f:
            h.dump_pickle(self.theta, f)
   
    def initialize_parameters(self, feature_dim=None, n_classes=None, corpus=None):
        """Initializes parameters for the SGD training with 0s.
        
        Parameters
        ----------
        feature_dim : int
        n_classes : int

        Returns
        -------
        None

        NOTES
        -----
        Assumes that the bias feature is 'baked in' to the features directly
        """

        assert bool(feature_dim and n_classes) ^ bool(corpus)

        if corpus:
            ixs = (d.label_ix for d in corpus)
            labels = (d.label for d in corpus)
            self.ix2label = dict(zip(ixs, labels))
            feature_dim = corpus[0].feature_vector.shape[0]
            n_classes = len(self.ix2label)

        self.feature_dim = feature_dim
        self.n_classes = n_classes
        self.theta = np.zeros((n_classes, feature_dim))

    def train(self, instances, dev_instances=None):
        """Construct a statistical model from labeled instances.

        NOTE: TAKES IN LIST OF DOCUMENT OBJECTS
        """
        if not instances:
            raise ValueError("len(instances) must be > 0")
        if not self.ix2label:
            _ixs = (d.label_ix for d in instances)
            _labels = (d.label for d in instances)
            self.ix2label = dict(zip(_ixs, _labels))
       
        # figure out feature dimensionality and no. of classes
        doc = instances[0]
        D = doc.feature_vector.shape[0]
        K = len(self.ix2label) 

        # initialize parameters
        self.initialize_parameters(feature_dim=D, n_classes=K)
        self.train_sgd(train_instances=instances, 
                       dev_instances=dev_instances, 
                       learning_rate=None, 
                       batch_size=None)

    def train_sgd(self, train_instances, dev_instances, 
                        learning_rate=None, batch_size=None):
        """Train MaxEnt model with Mini-batch Stochastic Gradient 

        NOTE: TAKES IN LIST OF DOCUMENT OBJECTS
        """
        if not learning_rate:
            learning_rate = self.LEARNING_RATE
        if not batch_size:
            batch_size = self.MINIBATCH_SIZE

        minibatch_generator = self.minibatcher(documents=train_instances, 
                                               M=batch_size)

        skip_dev_loss = bool(dev_instances is None)
        if not skip_dev_loss:
            Xdev, Ydev = self.get_data_matrix(dev_instances, as_onehot=True)


        while not self.converged():
            try:
                mb = next(minibatch_generator)
            except StopIteration:
                if self.VERBOSE:
                    print('Generating new minibatches')
                minibatch_generator = self.minibatcher(documents=train_instances,
                                                       M=batch_size)
                mb = next(minibatch_generator)

            X, Y = self.get_data_matrix(minibatch=mb, 
                                        as_onehot=True)

            # update theta
            grad = self.grad_loss(X,Y, lam=self.L2_PENALTY)
            self.theta = np.array(self.theta - learning_rate*grad)

            # calculate statistics and cache
            self.N_ITER += 1
            mb_train_loss = self.loss(X,Y, lam=self.L2_PENALTY)
            self.train_loss_cache.append(mb_train_loss)
            if not skip_dev_loss:
                dev_loss = self.loss(Xdev, Ydev, lam=self.L2_PENALTY)
                if len(self.dev_loss_improvement) == 0:
                    self.dev_loss_improvement = (np.diff(self.dev_loss_cache) >= self.TOL_LOSS).tolist()
                else:
                    delta_dev_loss = dev_loss - self.dev_loss_cache[-1] 
                    self.dev_loss_improvement.append(delta_dev_loss >= self.TOL_LOSS)
                self.dev_loss_cache.append(dev_loss)

            # we're not regularizing here so no need to truncate
            theta_norm = (self.theta**2).sum()
            self.theta_norm_cache.append(theta_norm)

            if self.VERBOSE:
                print('Status report')
                print('=======================================')
                print(f'After {self.N_ITER} iterations:')
                print(f'Minibatch train loss: {mb_train_loss}')
                if not skip_dev_loss:
                    print(f'Dev set loss: {dev_loss}')
                print(f'Theta norm: {theta_norm}')

        self.save_training_stats()
        try:
            self.plot_results()
        except:
            print('plotting failed')

        return self.theta


    def converged(self):
        """
        Monitors convergence of SGD via early stopping and self.MAX_ITER

        NOTE 
        ====
        Early stopping implemented according to the logic here https://anon.to/I61JSs
        """
        iterations_exceeded = bool(self.N_ITER >= self.MAX_ITER)
        if iterations_exceeded and self.VERBOSE:
            print('Iterations exceeded! => Stopping')
        if len(self.dev_loss_improvement) <= self.MAX_ITER_EARLY_STOP:  
            if self.VERBOSE:
                print('Haven\'t calculated enough dev loss values yet.')
                print('Current length is: {}'.format(len(self.dev_loss_improvement)))
                print('Current length of dev loss cache: {}'.format(len(self.dev_loss_cache)))
            should_stop_early = False
        else:
            should_stop_early = not any(self.dev_loss_improvement[-self.MAX_ITER_EARLY_STOP:])
            if should_stop_early and self.VERBOSE:
                print('No sufficient improvement in past dev losses. => early stop')

        if iterations_exceeded or should_stop_early:
            return True
        else:
            return False

    def minibatcher(self, documents, M=10):
        """
        Takes in a list of N documents, shuffles them around and 
        *yields* a minibatch of size M of them.

        Notes:
        - handles edge case where M does not evenly divide N
          by yielding remaining elements in a smaller minibatch.
        """
        N = len(documents)
        assert M <= N, "M must be strictly less than N"

        # copy documents so we don't shuffle originals
        docs_copy = documents[:]
        np.random.shuffle(docs_copy)
        ixs = (ix for ix in range(0, N, M))
        while True:
            try:
                start = next(ixs)
                yield docs_copy[start:start+M]
            except:
                break

    def get_data_matrix(self, minibatch, x_only=False, as_onehot=False):
        """
        Converts list of Document objects into a feature
        feature vector matrix X of dimension (D,N) and a label
        vector y.

        The label vector can be returned as a flat array of
        integer labels, or as a (K,N) dimensional matrix.
        
        Note: the matrix representation of y is mainly useful
        for gradient computation
        """
        corpus = minibatch[0].parent_corpus
        docids = [d.docid for d in minibatch]
        X = corpus.data_matrix[:, docids]
        if x_only:
            return X
        if as_onehot:
            y = np.vstack([d.label_onehot for d in minibatch]).T
        else:
            y = np.array([d.label_ix for d in minibatch])

        return X, y

    def _row_wise_multiply(self, v, M):
        """
        Multiplies each row of M by the
        corresponding element of v.

        Notes
        ------
        M : sparse matrix
        v : array-like

        Returns
        -------
        result : sparse matrix
        """

        # we need to handle the sparse case so as to
        # be able to construct the diagonal with diags
        # this is at most length K, so very cheap
        try:
            if v.shape[0] < v.shape[1]:
                v = v.T
            v = [m[0,0] for _, m in enumerate(v)]
        except:
            pass
        v_diag = sp.sparse.diags(v, 0)
        result = v_diag*M
        return result


    def loss(self, X, Y, lam=0.0):
        """
        Calculates logistic regression loss in matrix form.

        NOTES:
        ------
        Typically the loss is expressed as
            L = sum_i sum_c 1{y_i = c}*log(p(y_i=c|x_i, theta))

        We can represent this double sum as a sum across the
        elements of a matrix M :
            L = 1'M1  # (' denotes transpose)
        where
            M = Y*log(P) # log applied elementwise
            Y = KxN matrix of 1{y_i = c}
            P = KxN matrix of P(y_i = c | x_i)
        """
        if (self.theta == 0).all():
            self.theta += 1e-15
        n_observations = Y.shape[1]
        log_prob_matrix = self.probability_matrix(X, as_log=True)
        L = -1*(Y*log_prob_matrix).sum() # need -1 for NLL
        L /= n_observations # need to average so things don't blow up
        if lam > 0.0:
            theta_no_intercept = self.truncate(theta=self.theta, 
                                               remove_intercept=True)
            regularization_penalty = lam*((theta_no_intercept**2).sum())
            L += regularization_penalty
        return L

    def grad_loss(self, X, Y, lam=0.0):
        """
        Calculates gradient of loss function w.r.t. parametrs theta
        by multiplying observed data by the difference of observed
        and expected label counts.

        Parameters
        ----------
        X : sparse matrix of observed features
            total dimensions: (D, N)
        Y : sparse matrix of 1-hot encoded labels
            dimensions: (K, N)
        """
        P = self.probability_matrix(X)
        D = Y - P
        n_classes, n_observations = Y.shape
        output = None
        for i in range(n_observations):
            delta = D[:, i]
            feature_vector = X[:, i]
            stacked = self._stack_vertically(feature_vector, n_classes)
            M = self._row_wise_multiply(delta, stacked) 
            if output is not None:
                output = output + M
            else:
                output = M
        output *= -1 # we are minimizing so need NLL
        output /= n_observations # we need to AVERAGE to not blow up theta
        if lam > 0.0:
            output += lam*self.truncate(self.theta, remove_intercept=False)
        return output
    
    def _stack_vertically(self, v, k):
        """
        Stacks vector v on top of itself K times

        Note: operates directly on sparse matrices
        instead of taking in a Document object.
        """
        row_vector = bool(v.shape[0] < v.shape[1])
        if row_vector:
            M = sps.vstack([v for _ in range(k)])
        else:
            M = sps.hstack([v for _ in range(k)]).T
        return M

    def stack_feature_vector(doc, k):
        """
        Stacks feature vector of `doc` on top of itself k times
        """
        ids = [doc.docid for _ in range(k)]
        return doc.parent_corpus.data_matrix[:, ids].T

    def log_softmax(self, scores):
        """Transforms/squashes & normalizes score vector into probabilities"""
        return scores - logsumexp(scores, axis=0)

    def probability_matrix(self, X, as_log=False):
        """
        Calculates a KxN matrix of probability vectors
        where K = number of classes and N = number of observations.
        """
        scores = self.theta.dot(X.toarray())
        log_probs = self.log_softmax(scores)
        if as_log:
            return log_probs
        else:
            return np.exp(log_probs)

    def predict_proba(self, instances):
        """Calculates p(y|x, theta)"""
        X = self.get_data_matrix(instances, x_only=True)
        probs = self.probability_matrix(X)
        return probs

    def predict_log_proba(self, isntances):
        """Calculates log(p(y|x, theta))"""
        X = self.get_data_matrix(instances, x_only=True)
        log_probs = self.probability_matrix(X, as_log=True)
        return log_probs

    def predict(self, instances, as_label=True):
        """Takes a training example and predicts the most likely label/index"""
        prob = self.predict_proba(instances)
        best_ix = np.argmax(prob, axis=0)
        if as_label:
            return np.array([self.ix2label[ix] for ix in best_ix])
        else:
            return best_ix

    def classify(self, instances):
        """
        Classifies `instance` into one of the classes
        """
        return self.predict(instances, as_label=True)
