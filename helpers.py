import pickle
import scipy as sp
import numpy as np

def _row_wise_multiply(v, M):
    """
    Multiplies each row of M by the
    corresponding element of v.
    """
    v_diag = sp.sparse.diags(v, 0)
    result = v_diag*M
    return result

def load_pickle(p):
    with open(p, 'rb') as f:
        return pickle.load(f)

def dump_pickle(obj, p):
    with open(p, 'wb') as f:
        pickle.dump(obj, f)

