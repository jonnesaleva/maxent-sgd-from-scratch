from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.feature_extraction.text import CountVectorizer
from corpus import *
import scipy as sp
import numpy as np
import pickle

# constants
PREFIX = "../data/pa1/"
NAMES_LABELS_PATH = PREFIX + 'names_labels.pkl'
NAMES_FEATURES_PATH = PREFIX + 'names_bow_features.pkl'
REVIEWS_LABELS_PATH = PREFIX + 'reviews_labels.pkl'
REVIEWS_FEATURES_PATH = PREFIX + 'reviews_bow_features.pkl'

# function to generate ix2label mappings
def ix_label_mappings(corpus, lenc, onehot):
    """
    Takes in a corpus, and generates mappings for
        - document index => label index
        - document index => onehot vector
    """
    corpus_labels = np.array([n.label for n in corpus.documents])
    corpus_ix2label = {ix: l for ix, l in enumerate(corpus_labels)}
    corpus_label_ixs = {ix: label_ix for ix, label_ix 
                                    in enumerate(lenc.fit_transform(corpus_labels))}
    corpus_labels_onehot = {ix: onehot for ix, onehot 
                                      in enumerate(onehot.fit_transform(corpus_labels.reshape(-1, 1)))}
    return corpus_ix2label, corpus_label_ixs, corpus_labels_onehot

# feature extraction functions
def bag_of_unigrams(corpus,count_vectorizer):
    """Extracts bag-of-unigrams features for `corpus`.
        Note: for names, the unigrams are individual characters.
              for reviews, they are words.
    """
    documents = [doc.data for doc in corpus.documents]
    corpus_tdm = count_vectorizer.fit_transform(documents)
    return corpus_tdm

def bias_feature(corpus):
    """Generates bias feature"""
    corpus_bias = np.ones(shape=(len(corpus), 1))
    return corpus_bias

def extract_features(corpus, count_vectorizer):
    """Extracts bias and bag-of-unirgams for `corpus`."""
    corpus_bias = bias_feature(corpus)
    corpus_tdm = bag_of_unigrams(corpus,count_vectorizer) 
    corpus_features = sp.sparse\
                        .hstack([corpus_bias, corpus_tdm])\
                        .T.tocsc()
    return corpus_features

def dump_pickle(p, data):
    """
    Pickle `data` to disk.
    """
    with open(p, 'wb') as f:
        pickle.dump(data, f)

### NAMES CORPUS ###
print('loading names corpus......')
names = NamesCorpus(feature_extraction_mode=True)
cv_names = CountVectorizer(analyzer='char')
lenc_names = LabelEncoder()
onehot_names = OneHotEncoder()
print('done... calculating indices....')
names_ix2label, names_label_ixs, names_labels_onehot = ix_label_mappings(names, lenc_names, onehot_names)
names_indices = {
    'ix2label': names_ix2label,
    'label_ixs': names_label_ixs,
    'labels_onehot': names_labels_onehot
}
print('done... generating features....')
names_features = extract_features(names, cv_names)

# dump indices
print('done... pickling generated data...')
dump_pickle(NAMES_LABELS_PATH, names_indices)
dump_pickle(NAMES_FEATURES_PATH, names_features)
print('all done!')

### REVIEW_CORPUS ###
print('loading reviews corpus......')
reviews = ReviewCorpus('yelp_reviews.json', feature_extraction_mode=True)
cv_reviews = CountVectorizer()
lenc_reviews = LabelEncoder()
onehot_reviews = OneHotEncoder()
print('done... calculating indices....')
reviews_ix2label, reviews_label_ixs, reviews_labels_onehot = ix_label_mappings(reviews, lenc_reviews, onehot_reviews)
reviews_indices = {
    'ix2label': reviews_ix2label,
    'label_ixs': reviews_label_ixs,
    'labels_onehot': reviews_labels_onehot 
}

print('done... generating features....')
reviews_features = extract_features(reviews, cv_reviews)

# make some kimchi
print('done... pickling generated data...')
dump_pickle(REVIEWS_LABELS_PATH, reviews_indices)
dump_pickle(REVIEWS_FEATURES_PATH, reviews_features)

print('all done!')
